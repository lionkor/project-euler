#include <iostream>

int main() {
    size_t sum {};
    for (size_t i = 3; i < 1000; ++i) {
        if (i % 3 == 0 || i % 5 == 0) {
            sum += i;
        }
    }
    std::cout << sum << "\n";
}
