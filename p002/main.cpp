#include <iostream>
#include <tuple>

inline auto fib(size_t a, size_t b) {
    return std::make_tuple(b, a + b);
}

int main() {
    size_t k = 1, i = 1;
    size_t sum {};
    while (k <= 4'000'000) {
        if (k % 2 == 0) {
            sum += k;
        }
        std::tie(i, k) = fib(i, k);
    }
    std::cout << sum << "\n";
}
