#include <algorithm>
#include <cmath>
#include <iostream>
#include <set>

inline bool is_prime(size_t n) {
    for (size_t i = 2; i < size_t(std::sqrt(n)) + 1; ++i) {
        if (n % i == 0) {
            return false;
        }
    }
    return true;
}

std::set<size_t> prime_factors(size_t n) {
    std::set<size_t> factors;
    size_t div = 2;
    for (;;) {
        if (n % div == 0) {
            if (is_prime(div)) {
                factors.insert(div);
            }
            n /= div;
        } else {
            if (is_prime(n)) {
                factors.insert(n);
                break; // done
            }
            do {
                ++div;
            } while (!is_prime(div));
        }
    }
    return factors;
}

int main() {
    const size_t n = 600851475143;
    auto factors = prime_factors(n);
    auto max_iter = std::max_element(factors.begin(), factors.end());
    if (max_iter != factors.end()) {
        std::cout << *max_iter << "\n";
    }
}
